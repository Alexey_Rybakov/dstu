package com.example.dstu.di

import com.example.dstu.MainActivity
import com.example.dstu.presentation.view.group_choose.GroupChooseController
import com.example.dstu.presentation.view.schedule.ScheduleController
import dagger.Component
import javax.inject.Singleton


@Component(modules = arrayOf(ApplicationModule::class))
@Singleton
interface AppComponent {
    fun inject(activity: MainActivity)
    fun inject(controller: GroupChooseController)
    fun inject(controller: ScheduleController)

}