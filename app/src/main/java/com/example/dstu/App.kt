package com.example.dstu

import android.app.Application
import com.example.dstu.di.AppComponent
import com.example.dstu.di.ApplicationModule
import com.example.dstu.di.DaggerAppComponent
import io.paperdb.Paper


class App:Application(){
    lateinit var appComponent: AppComponent;
    override fun onCreate() {
        super.onCreate()
        appComponent = buildComponent();
        Paper.init(applicationContext);
    }
    private fun buildComponent(): AppComponent {
        return DaggerAppComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }
}

