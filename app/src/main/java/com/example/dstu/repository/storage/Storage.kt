package com.example.dstu.repository.storage

import com.example.dstu.models.storage.LessonsTime
import com.example.dstu.models.storage.Schedule
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import io.paperdb.Paper
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import javax.inject.Inject

class Storage @Inject constructor(val moshi: Moshi){
    companion object {
        private const val CACHE_BOOK_NAME = "CACHE_BOOK_NAME"
    }

    fun save(schedule: Schedule): Completable {
        return Completable.fromCallable({
            Paper.book(CACHE_BOOK_NAME).write(schedule.group.toUpperCase(), schedule);
        })
    }
    fun get(group: String): Maybe<Schedule> {
        return Maybe.create({
            val s:Schedule? = Paper.book(CACHE_BOOK_NAME).read(group.toUpperCase());
            if (s==null) it.onComplete() else it.onSuccess(s)
        })
    }

    fun getCacheKeys(): Single<List<String>>{
        return Single.fromCallable { Paper.book(CACHE_BOOK_NAME).allKeys }
    }

    fun getLessonsTime(): Single<List<LessonsTime>>{
        return Single.fromCallable {
            val type = Types.newParameterizedType(List::class.java, LessonsTime::class.java)
            val adapter:JsonAdapter<List<LessonsTime>> = moshi.adapter(type)
            val times = adapter.fromJson(lessonsTime)
            return@fromCallable times;
        }
    }
    /**
     * Расписание звонков, временно
     */
    val lessonsTime = " [\n" +
            "    {\"number\":\"1\",\"start_time\":\"08:30\",\"end_time\":\"10:05\",\"time_interval\":\"08:30-10:05\"},{\"number\":\"2\",\"start_time\":\"10:15\",\"end_time\":\"11:50\",\"time_interval\":\"10:15-11:50\"},{\"number\":\"3\",\"start_time\":\"12:00\",\"end_time\":\"13:35\",\"time_interval\":\"12:00-13:35\"},{\"number\":\"4\",\"start_time\":\"14:15\",\"end_time\":\"15:50\",\"time_interval\":\"14:15-15:50\"},{\"number\":\"5\",\"start_time\":\"16:00\",\"end_time\":\"17:35\",\"time_interval\":\"16:00-17:35\"},{\"number\":\"6\",\"start_time\":\"17:45\",\"end_time\":\"19:20\",\"time_interval\":\"17:45-19:20\"},{\"number\":\"7\",\"start_time\":\"19:30\",\"end_time\":\"21:05\",\"time_interval\":\"19:30-21:05\"}\n" +
            "  ]"
}