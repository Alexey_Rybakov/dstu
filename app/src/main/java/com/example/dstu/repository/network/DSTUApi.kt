package com.example.dstu.repository.network

import com.example.dstu.models.network.CurrentWeek
import com.example.dstu.models.network.ScheduleWrapper
import com.example.dstu.models.network.SearchScheduleItem
import io.reactivex.Single
import retrofit2.http.*


interface DSTUApi{

    @GET("/site/ci/smartphone_api/getCurrentWeek")
    fun getCurrentWeek(): Single<CurrentWeek>


    @GET("/site/ci/time_table/quickSearchJSON/{input}")
    fun quickSearch(@Path("input") input:String): Single<List<SearchScheduleItem>>

    /**
     * Получение расписание
     * @param type - тип расписания(1 - обычное, 2 - экзаминационное, 3 - установочная сессия(для заочников) и т.д.)
     * @param period_fk - это период обучения(не обязательно передавать)
     * @param arg_field - тип расписания (group)
     * @param arg_value - значение объекта расписания(id группы, или название, или id пользователя и т.д.)
     * @return
     */
    @FormUrlEncoded
    @POST("/site/ci/smartphone_api/getTimeTable/{type}")
    fun getTeacherSchedule(
            @Path("type") type:Int,
            @Field("period_fk") period_fk:String,
            @Field("field")arg_field:String,
            @Field("value")arg_value:String
    ): Single<ScheduleWrapper>

}