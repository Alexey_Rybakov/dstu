package com.example.dstu.repository

import com.example.dstu.models.network.CurrentWeek
import com.example.dstu.models.network.SearchScheduleItem
import com.example.dstu.models.presentation.ScheduleItem
import com.example.dstu.models.storage.Schedule
import com.example.dstu.repository.network.DSTUApi
import com.example.dstu.repository.storage.Storage
import com.squareup.moshi.Moshi
import io.reactivex.Single
import java.util.*
import javax.inject.Inject


class DSTURepository @Inject constructor(private val api: DSTUApi) {
    private val storage: Storage = Storage(Moshi.Builder().build());
    fun getCurrentWeek(): Single<CurrentWeek>{
        return api.getCurrentWeek();
    }
    fun getSavedKeys(): Single<List<String>>{
        return storage.getCacheKeys();
    }
    fun quickSearch(token: String) :Single<List<SearchScheduleItem>>{
        return api.quickSearch(token);
    }
    fun getTeacherSchedule(group: String) : Single<List<ScheduleItem>>{
        return storage.get(group)
                .map({it.schedule})
                .switchIfEmpty( api.getTeacherSchedule(1,"","group",group)
                        .map({return@map it.aTT})
                        .doOnSuccess({
                            storage.save(Schedule(group,Date(),it)).subscribe()
                        })
                )
    }
    fun getLessonsTime() = storage.getLessonsTime();
}