package com.example.dstu

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.bluelinelabs.conductor.Conductor
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.example.dstu.databinding.ActivityMainBinding
import com.example.dstu.presentation.view.group_choose.GroupChooseController
import com.example.dstu.repository.DSTURepository
import javax.inject.Inject


class MainActivity : AppCompatActivity() {

    @Inject lateinit var repo:DSTURepository;

    lateinit var binding: ActivityMainBinding;
    lateinit var router: Router;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        (applicationContext as App).appComponent.inject(this);

        router = Conductor.attachRouter(this, binding.root, savedInstanceState)
        if (!router.hasRootController()) {
            router.setRoot(RouterTransaction.with(GroupChooseController()))
        }
    }

    override fun onBackPressed() {
        if (!router.handleBack()) {
            super.onBackPressed()
        }
    }
}


