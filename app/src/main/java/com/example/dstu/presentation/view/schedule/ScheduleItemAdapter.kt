package com.example.dstu.presentation.view.schedule

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.dstu.databinding.ScheduleItemBinding
import com.example.dstu.databinding.ScheduleItemHeaderBinding
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter


class ScheduleItemAdapter: RecyclerView.Adapter<ViewHolder>(), StickyRecyclerHeadersAdapter<HeaderViewHolder> {

    private val data = ArrayList<ScheduleController.PresentItem>();

    fun setData(d: List<ScheduleController.PresentItem>){
        data.clear();
        data.addAll(d);
        for (presentItem in data) {
            Log.d("TAG"," " + presentItem.scheduleItem.day)
        }
        notifyDataSetChanged();
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        return ViewHolder(ScheduleItemBinding.inflate(LayoutInflater.from(parent!!.context),parent,false));
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val item = data[position];
        val b = holder!!.b;
        b.interval.text = item.lessonsTime.start_time + "\n" + item.lessonsTime.end_time;
        b.schTime.text = item.scheduleItem.num_les.toString();
        b.subject.text = item.scheduleItem.subject;
        b.room.text = item.scheduleItem.aud;
    }

    override fun getItemCount() = data.size;

    override fun getHeaderId(position: Int): Long {
        return data[position].scheduleItem.day.toLong()
    }
    private val days = arrayOf("Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота");
    override fun onCreateHeaderViewHolder(parent: ViewGroup?): HeaderViewHolder {
        return HeaderViewHolder(ScheduleItemHeaderBinding.inflate(LayoutInflater.from(parent!!.context),parent,false));
    }

    override fun onBindHeaderViewHolder(holder: HeaderViewHolder?, position: Int) {
        holder!!.b.text.text = days[data[position].scheduleItem.day];
    }
}

class ViewHolder(val b: ScheduleItemBinding): RecyclerView.ViewHolder(b.root);
class HeaderViewHolder(val b: ScheduleItemHeaderBinding): RecyclerView.ViewHolder(b.root)