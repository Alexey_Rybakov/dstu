package com.example.dstu.presentation.view.schedule


interface ChildDataReceiver {
    fun update(data: List<ScheduleController.PresentItem>);
}