package com.example.dstu.presentation.view.schedule

import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bluelinelabs.conductor.Controller
import com.example.dstu.databinding.ScheduleChildLayoutBinding
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration


class ScheduleChildController: Controller(), ChildDataReceiver{



    override fun update(data: List<ScheduleController.PresentItem>) {
        adapter.setData(data);
    }

    lateinit var binding: ScheduleChildLayoutBinding;
    lateinit var adapter: ScheduleItemAdapter;
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        binding = ScheduleChildLayoutBinding.inflate(inflater,container,false);
        binding.recyclerView.layoutManager = LinearLayoutManager(applicationContext);
        adapter = ScheduleItemAdapter();
        binding.recyclerView.addItemDecoration(StickyRecyclerHeadersDecoration(adapter))
        binding.recyclerView.adapter = adapter;
        return binding.root;
    }


}