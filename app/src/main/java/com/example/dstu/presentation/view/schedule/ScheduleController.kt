package com.example.dstu.presentation.view.schedule

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.support.RouterPagerAdapter
import com.example.dstu.App
import com.example.dstu.BundleBuilder
import com.example.dstu.databinding.ScheduleLayoutBinding
import com.example.dstu.models.presentation.ScheduleItem
import com.example.dstu.models.storage.LessonsTime
import com.example.dstu.presentation.view.group_choose.GroupChooseController
import com.example.dstu.repository.DSTURepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

class ScheduleController constructor(group: Bundle) : Controller(group) {
    /*
     * Для восстановления контроллера после десериализации необходимо: при создании передавать параметры
     * в Bundle конструктору суперкласса,
     * определить дефолтный конструктор или конструктор с Bundle у класса наследника
     *
     * Вторичный конструктор должен вызывать первичный конструктор класса
     */
    constructor(group: String): this(BundleBuilder().putString("group",group).build());

    lateinit var group:String;
    lateinit var binging: ScheduleLayoutBinding;
    lateinit var pagerAdapter: RouterPagerAdapter;
    lateinit @Inject var repo: DSTURepository;
    lateinit var firstPage: ScheduleChildController;
    lateinit var secondPage: ScheduleChildController;

    val compositDisposable = CompositeDisposable();
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        binging = ScheduleLayoutBinding.inflate(inflater,container,false);
        (applicationContext as App).appComponent.inject(this);
        group = args.getString("group");
        val tabs = arrayOf("Верхняя","Нижняя");
        firstPage = ScheduleChildController();
        secondPage = ScheduleChildController();
        pagerAdapter = object : RouterPagerAdapter(this){
            override fun getCount(): Int {
                return tabs.size;
            }
            override fun configureRouter(router: Router, position: Int) {
                if (!router.hasRootController()) {
                    router.setRoot(RouterTransaction.with(if (position==0) firstPage else secondPage))
                }
            }
            override fun getPageTitle(position: Int): CharSequence {
                return tabs[position];
            }
        }
        binging.tabLayout.setupWithViewPager(binging.pager);
        binging.pager.adapter = pagerAdapter;
        binging.toolbar.setTitleTextColor(Color.WHITE)
        binging.toolbar.setTitle(group);
        return binging.root;
    }
    override fun onAttach(view: View) {
        super.onAttach(view)
        loadData(group);
    }
    override fun onDetach(view: View) {
        compositDisposable.clear();
        super.onDetach(view);
    }
    private fun loadData(group: String){
        compositDisposable.add(
                Single.zip(
                        repo.getLessonsTime(),
                        repo.getTeacherSchedule(group),
                        BiFunction{ timeLessons:List<LessonsTime>, schedule:List<ScheduleItem> ->
                            return@BiFunction Pair(timeLessons,schedule);
                        }
                )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableSingleObserver<Pair<List<LessonsTime>, List<ScheduleItem>>>() {
                            override fun onSuccess(data: Pair<List<LessonsTime>, List<ScheduleItem>>) {
                                    processScheduleData(data);
                            }

                            override fun onError(e: Throwable) {
                                e.printStackTrace();
                                Toast.makeText(applicationContext,"Ошибка при загрузке данных",Toast.LENGTH_SHORT).show();
                            }
                        }
                )
        )
    }
    data class PresentItem(val lessonsTime: LessonsTime,val scheduleItem: ScheduleItem): Comparable<PresentItem>{
        override fun compareTo(other: PresentItem): Int {
            var result = this.scheduleItem.day.compareTo(other.scheduleItem.day);
            if (result == 0){
                result = this.lessonsTime.number.compareTo(other.lessonsTime.number);
            }
            return result;
        }

        override fun equals(other: Any?): Boolean {
            if (this === other)
                return true
            if (other == null)
                return false
            if (javaClass != other.javaClass)
                return false
            val other = other as PresentItem
            if (this.scheduleItem.day != other.scheduleItem.day)
                return false
            if (this.scheduleItem.num_les != other.scheduleItem.num_les)
                return false
            if (this.scheduleItem.week_num != other.scheduleItem.week_num)
                return false
            return true
        }

        override fun hashCode(): Int {
            val prime = 31
            var result = 1
            result = prime * result + scheduleItem.day
            result = prime * result + scheduleItem.num_les
            result = prime * result + scheduleItem.week_num
            return result
        }
    }

    private fun processScheduleData(data: Pair<List<LessonsTime>, List<ScheduleItem>>){
        val firstWeek = TreeSet<PresentItem>();
        val secondWeek = TreeSet<PresentItem>();
        for (scheduleItem in data.second) {
            val presentItem = PresentItem(data.first[scheduleItem.num_les],scheduleItem);
            if (scheduleItem.week_num==0){
                firstWeek.add(presentItem);
                secondWeek.add(presentItem);
            }else if(scheduleItem.week_num==1){
                firstWeek.add(presentItem);
            }else if(scheduleItem.week_num==2){
                secondWeek.add(presentItem);
            }
        }
        val firstWeekL = firstWeek.toList();
        val secondWeekL = secondWeek.toList();
        Collections.sort(firstWeekL);
        Collections.sort(secondWeekL);
        firstPage.update(firstWeekL);
        secondPage.update(secondWeekL);
    }

    override fun handleBack(): Boolean {
        router.setRoot(RouterTransaction.with(GroupChooseController()));
        return true;
    }
}