package com.example.dstu.presentation.view.group_choose

import android.databinding.DataBindingUtil
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.RouterTransaction
import com.example.dstu.App
import com.example.dstu.BR
import com.example.dstu.R
import com.example.dstu.databinding.GroupChooseControllerLayoutBinding
import com.example.dstu.databinding.GroupItemBinding
import com.example.dstu.presentation.view.schedule.ScheduleController
import com.example.dstu.repository.DSTURepository
import com.github.nitrico.lastadapter.LastAdapter
import com.github.nitrico.lastadapter.Type
import com.miguelcatalan.materialsearchview.MaterialSearchView
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.collections.ArrayList




class GroupChooseController: Controller() {
    lateinit var binding:GroupChooseControllerLayoutBinding;
    @Inject lateinit var repo: DSTURepository;
    var adapterData = ArrayList<String>();
    lateinit var adapter: LastAdapter;
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        (applicationContext as App).appComponent.inject(this);

        binding = DataBindingUtil.inflate(inflater, R.layout.group_choose_controller_layout,container,false);
        binding.toolbar.inflateMenu(R.menu.search);
        binding.toolbar.setOnMenuItemClickListener(Toolbar.OnMenuItemClickListener {
            if(it.itemId==R.id.action_search){
                binding.searchView.showSearch(true);
            }
            return@OnMenuItemClickListener false;
        });
        binding.listResults.addItemDecoration(HorizontalDividerItemDecoration.Builder(applicationContext).build())
        val type = Type<GroupItemBinding>(R.layout.group_item)
                .onClick{
                    var item = it.binding.item;
                    if (item != null){
                        openSchedule(item);
                    }
                }
        adapterData = ArrayList<String>();
        adapter = LastAdapter(adapterData, BR.item)
                .map<String>(R.layout.group_item)
                .type{
                    item, position ->type
                }
                .into(binding.listResults)

        binding.listResults.layoutManager = LinearLayoutManager(applicationContext);
        binding.listResults.adapter = adapter;
        fromSearchView(binding.searchView)
                .debounce(300, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .switchMap({ s ->
                    if (s.isEmpty()){
                        return@switchMap repo.getSavedKeys()
                                .toObservable();
                    }else{
                        return@switchMap repo.quickSearch(s)
                                .subscribeOn(Schedulers.io())
                                .doOnDispose {
                                    Log.d("TAG","Call disposed "+s)
                                }
                                .doOnSuccess({
                                    Log.d("TAG","Call finished "+it)
                                })
                                .onErrorReturnItem(Collections.emptyList())
                                .map({
                                    var l = ArrayList<String>(it.size);
                                    for (searchScheduleItem in it) {
                                        l.add(searchScheduleItem.title_field)
                                    }
                                    return@map l;
                                })
                                .toObservable()
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object :DisposableObserver<List<String>>(){
                        override fun onNext(t: List<String>) {
                            Log.d("TAG","Call published "+t)
                            setSearchResults(t);
                        }
                        override fun onError(e: Throwable) {
                                e.printStackTrace()
                        }

                        override fun onComplete() {
                            Log.i("TAG","OnComplete")
                        }
                    })
        binding.searchView.setQuery("",false)
        return binding.root;
    }
    fun setSearchResults(res: List<String>){
        adapterData.clear();
        adapterData.addAll(res)
        adapter.notifyDataSetChanged()
    }
    fun fromSearchView(searchView: MaterialSearchView): Observable<String> {
        val subject = PublishSubject.create<String>()
        searchView.setOnQueryTextListener(object : MaterialSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                subject.onComplete();
                return true;
            }

            override fun onQueryTextChange(newText: String): Boolean {
                subject.onNext(newText);
                return true;
            }
        })
        return subject;
    }

    fun openSchedule(group: String){
        router.setRoot(RouterTransaction.with(ScheduleController(group)))
    }


}
