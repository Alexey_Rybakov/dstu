package com.example.dstu.models.presentation

class ScheduleItem(
        val day : Int,
        val num_les : Int,
        val week_num : Int,
        val type_week : Int?,
        val subject : String?,
        val type : String?,
        val aud : String?,
        val teacher : String?,
        val group : String?
);