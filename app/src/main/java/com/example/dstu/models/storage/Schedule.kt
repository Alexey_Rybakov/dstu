package com.example.dstu.models.storage

import com.example.dstu.models.presentation.ScheduleItem
import java.util.*

class Schedule(val group: String,val date: Date,val schedule: List<ScheduleItem>)
