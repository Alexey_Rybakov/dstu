package com.example.dstu.models.network

import com.example.dstu.models.presentation.ScheduleItem


class ScheduleWrapper(val mobileErr: String?,
                      val period_fk: Int,
                      val trange_fk: Int,
                      val aAttrTT: A_AttrTT,
                      val aGroups: List<Group>,
                      val aTT: List<ScheduleItem>);
class Group(val group : String);
class A_TT(
        val day : Int?,
        val num_les : Int?,
        val week_num : Int?,
        val type_week : Int?,
        val subject : String?,
        val type : String?,
        val aud : String?,
        val teacher : String?,
        val group : String?,
        val person_fk : Int?,
        val id : Long?
);
class A_AttrTT(val aCurrentOptions: A_CurrentOptions, val aTimeLesson:List<A_TimeLesson>);
class A_TimeLesson(val number : Int,
                   val start_time : String,
                   val end_time : String,
                   val time_interval : String);
class A_CurrentOptions(val nowWeek : Int);